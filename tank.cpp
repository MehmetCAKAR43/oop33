#include"tank.h"
#include<iostream>
#include<fstream>
#include "readfile.h"

using namespace std;

int number_of_tank = 0;
tank::tank(double capacity_ = 0.1)
{
	static int counter = 0;
	capacity = capacity_;
	number_of_tank++;
	id = counter;
	counter++;
}

tank::~tank()
{
	number_of_tank--;
}

bool tank::is_valve_open()
{
	return valve;
}

void tank::open_valve()
{
	valve = 1;
	readfile::get_output_name() << "Tank | " << id << " |'s valve is open now." << endl;
}

void tank::close_valve()
{
	valve = 0;
	readfile::get_output_name() << "Tank | " << id << " |'s valve is close now." << endl;
}

void tank::break_fuel_tank()
{
	broken = 1;
	readfile::get_output_name() << "Tank | " << id << " | is broken now." << endl;
	
}

void tank::repair_fuel_tank()
{
	broken = 0;
	readfile::get_output_name() << "Tank | " << id << " | is repaired now." << endl;

}

void tank::use_fuel()
{
	fuel_quantity -= 5.5;
}
double tank::get_fuel()
{
	return fuel_quantity;
}

void tank::print_info()
{
	readfile::get_output_name() <<"capacity: " <<capacity << " | fuel quantity: " << fuel_quantity << " | ";
	if (broken == 0)
	{
		readfile::get_output_name() << "tank is not broken"<<endl;
	}
	else
	{
		readfile::get_output_name() << "tank is broken"<<endl;
	}
}

void tank::fill_fuel(double fuel_cap)
{
	
	if (fuel_cap+fuel_quantity>capacity+0.001)
	{
		fuel_quantity = capacity;
		readfile::get_output_name() << "Tank capacity exceeded, tank is its full capacity tank | ";
	}
	else
	{
		readfile::get_output_name() << "Fuel added to tank | ";
		fuel_quantity += fuel_cap;
	}
}
void tank::set_fuel(double fuel_cap)
{
	fuel_quantity = fuel_cap;
}
void tank::connect() {
	tank_is_connected = 1;
}
void tank::disconnect() {
	tank_is_connected = 0;
}
bool tank::is_connect() {
	return tank_is_connected;
}
int tank::get_id() {
	return id;
}
void tank::stop_info(string message)
{
	readfile::get_output_name() <<"Tank | "<<id<<" |"<< message;
	readfile::get_output_name() << "Valve | " << id << " |" << message << endl;
}

void tank::removeObserver(Observable* observable)
{
	observable->removeObserver(this);
}

void tank::setObservable(Observable* observable)
{
	this->observable = observable;
}

