#pragma once
#include<stdio.h>
#include<vector>
#include"tank.h"
#include"Observer.h"
#include "Observable.h"
using namespace std;
class engine:public Observer
{
	Observable* observable;
	const double fuel_per_second=5.5;
	bool engineIsworking=false;
	int sayac = 0;
	int usin = 0;
public:
	vector<tank> tanks;

	engine();
	void add_fuel_tank(double);
	void list_fuel_tank();
	void remove_fuel_tank(int);
	void connect_fuel_tank_to_engine(int);
	void disconnect_fuel_tank_from_engine(int);
	void start_engine();
	void stop_engine();
	//void absorb_fuel(double);
	void print_fuel_tank_count();
	void give_back_fuel(double);
	void list_connected_tanks();
	void print_total_fuel_quantity();
	void print_total_consumed_fuel_quantity();
	void print_tank_info(int);
	void fill_tank(int,double);
	void use_fuel();
	void wait(int);
	void stop_info(string);
	void removeObserver(Observable* observable);
	void setObservable(Observable* observable);
};
