#pragma once
#include "Observable.h"
#include<list>
using namespace std;
class Notice_Stop_Observable :public Observable
{
	std::list <Observer*> list;
	string message_coming="Simulation stopped\n";
public:
	void addObserver(Observer* observer)
    {
        list.push_back(observer);
    }   
    void removeObserver(Observer* observer) 
    {
        list.remove(observer);
    }
    void notify_stop() {
        for (int i = 0; i < list.size(); i++)
        {
            list.front()->stop_info(message_coming);
            list.pop_front();
        }
    }
};

