#include"engine.h"
#include<iostream>
#include<fstream>
#include<vector>
#include<stdlib.h>
#include"tank.h"
#include "readfile.h"

using namespace std;
engine::engine()
{
	add_fuel_tank(55);
	tanks.at(0).connect();
}
void engine::add_fuel_tank(double capa){
	tank tank1(capa);
	bool flag = false;
	static int tank_id_count = 0;
	tanks.push_back(tank1);
		if (tanks.size()!=1)
		{
			readfile::get_output_name() << "Tank | " << ++tank_id_count << " | added!" << endl;
		}
	
	if (engineIsworking)
	{
		use_fuel();
	}
}
void engine::list_fuel_tank() {
	readfile::get_output_name() << "Tank id's : | ";
	for (int i = 1; i < tanks.size(); i++)
	{
		readfile::get_output_name() << tanks.at(i).get_id() << " | ";
	}
	readfile::get_output_name() << endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}
void engine::remove_fuel_tank(int id) {
	bool flag = 0;
	for (int i = 1; i < tanks.size(); i++) {
		if (tanks.at(i).get_id() == id) {
			tanks.erase(tanks.begin() + i);
			readfile::get_output_name() << "Tank | " << id << " | is removed!!" << endl;
			flag = 1;
			break;
		}
	}
	if(flag==0)
	readfile::get_output_name() << "Tank could not find in there!" << endl;
	usin = 0;
	if (engineIsworking)
	{
		use_fuel();
	}
}
void engine::connect_fuel_tank_to_engine(int id) {
	bool flag = false;
	for (int i = 0; i <tanks.size(); i++)
	{
		if (tanks.at(i).get_id() == id) {
			if (tanks.at(i).is_connect() == 0) {
				if (tanks.at(i).is_valve_open() == 1) {
					tanks.at(i).connect();
				}
				else {
					readfile::get_output_name() << "Valve is close. Tank | " << id << " | cannot connect" << endl;
				}

			}
			else {
				readfile::get_output_name() << "Tank | " << id << " |  is already connected." << endl;
			}
			flag = true;
			break;
		}
	
	}
	if(!flag)
		readfile::get_output_name() << "Tank | " << id << " |  is not found." << endl;

	if (engineIsworking)
	{
		use_fuel();
	}

}
void engine::disconnect_fuel_tank_from_engine(int id) {
	bool flag = false;
	for (int i = 0; i < tanks.size(); i++)
	{
		if (tanks.at(i).get_id() == id) {
			if (tanks.at(i).is_connect() == 1) {
				tanks.at(i).disconnect();

			}
			else {
				readfile::get_output_name() << " Tank | " << id << " |  is already disconnected." << endl;
			}
			flag = true;
			break;
		}

	}
	if (!flag)
		readfile::get_output_name() << " Tank | " << id << " |  is not found." << endl;
	
	if (engineIsworking)
	{
		use_fuel();
	}
}
void engine::start_engine() {
	bool flag = 0;
	for (int i = 1; i < tanks.size(); i++)
	{
		if (tanks.at(i).is_connect()) {
			if (this->engineIsworking == true)
			{
				readfile::get_output_name() << "Engine has already working!" << endl;
			}
			else
			{
				this->engineIsworking = true;
				readfile::get_output_name() << "Engine is start to work!" << endl;
			}
			flag = 1;
			break;
		}
	}
	
	if(!flag)
	{
		readfile::get_output_name() << "There is no tank connected to engine, engine is not start to work!" << endl;
	}
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::stop_engine(){
	if (this->engineIsworking == false)
	{
		readfile::get_output_name() << "Engine has already stopped!" << endl;
	}
	else
	{
		this->engineIsworking = false;
		readfile::get_output_name() << "Engine is stopping at the moment!" << endl;
		give_back_fuel(tanks.at(0).get_fuel());
	}
	
}

/*
void engine::absorb_fuel(double x){
	readfile::get_output_name() << x << endl;
}*/

void engine::give_back_fuel(double y){
	double min = 999;
	int id = 0;
	bool flag = 0;
	for (int i = 1; i < tanks.size(); i++) {
		if (tanks.at(i).get_fuel() < min) {
			min = tanks.at(i).get_fuel();
			id = tanks.at(i).get_id();
			flag = 1;
		}

	}
	if (flag==0)
	{
		tanks.at(id).set_fuel(tanks.at(id).get_fuel() + y);
		tanks.at(0).set_fuel(tanks.at(id).get_fuel() - y);
	}
	readfile::get_output_name() << y << " amount of fuel given to tank | " << id <<" |"<< endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::print_fuel_tank_count() 
{
	
	readfile::get_output_name() << "Number of tank: " << tanks.size() << endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}


void engine::list_connected_tanks()
{
	bool flag = 0;
	readfile::get_output_name() << "These are connected tanks: | ";
	for (int i = 1; i < tanks.size(); i++)
	{
		if (tanks.at(i).is_connect())
		{
			readfile::get_output_name() << tanks.at(i).get_id() << " | ";
			flag = 1;
		}	
	}
	if (!flag)
	{
		readfile::get_output_name() << "none!";
	}
	readfile::get_output_name() << endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::print_total_fuel_quantity() 
{
	double toplam=0;
	for (int i = 0; i < tanks.size(); i++)
	{
		toplam += tanks.at(i).get_fuel();
	}
	readfile::get_output_name() << "Total fuel quantity: " << toplam<<endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::print_total_consumed_fuel_quantity() 
{
	readfile::get_output_name() <<"Total consumed fuel quantity: " <<sayac * 5.5<<endl;
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::print_tank_info(int id) 
{
	bool flag = 0;
	for (int i = 1; i < tanks.size(); i++) {
		if (tanks.at(i).get_id() == id)
		{
			readfile::get_output_name() << "Tank " << id << " -> ";
			tanks.at(i).print_info();
			flag = 1;
			break;
		}
	}
	if(!flag)
	{
			readfile::get_output_name() << "Tank doesnt exist!" << endl;
	}
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::fill_tank(int id , double b) 
{
	bool flag = 0;
	for (int i = 1; i < tanks.size();i++) {
		if (tanks.at(i).get_id() == id)
		{
			tanks.at(i).fill_fuel(b);
			usin = 0;
			readfile::get_output_name() << id << " |" << endl;
			flag = 1;
			break;
		}
	}
	if(!flag)
	{
		readfile::get_output_name() << "Tank doesnt exist!" << endl;
	}
	
	if (engineIsworking)
	{
		use_fuel();
	}
}

void engine::use_fuel()
{ 
	int sayac1 = 0;
	sayac++;
	while (1) {
		if (usin == 0) {
			if (tanks.at(usin).get_fuel() > 20) {
				tanks.at(usin).use_fuel();
				break;
			}
			else {
				usin++;
			}
		}
		else if (tanks.at(usin).is_connect()) {
			if (tanks.at(usin).get_fuel() >= 5.5) {
				tanks.at(usin).use_fuel();
				break;
			}
			else {
				usin++;
			}
		}
		else if (usin==tanks.size()) {
			readfile::get_output_name() << "There is no fuel in the any tanks!!" << endl;
			stop_engine();
			break;
		}
	}
	
}

void engine::wait(int sec)
{
	for (int i = 0; i < sec; i++)
	{
		use_fuel();
	}
}

void engine::stop_info(string message)
{
	readfile::get_output_name() <<"Engine" <<message<<endl;
}

void engine::removeObserver(Observable* observable)
{
	observable->removeObserver(this);
}

void engine::setObservable(Observable* observable)
{
	this->observable = observable;
}
