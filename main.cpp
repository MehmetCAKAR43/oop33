#include<iostream>
#include"engine.h"
#include"tank.h"
#include"readfile.h"
#include<stdio.h>
#include<vector>
#include <sstream>
#include"Notice_Stop_Observable.h"
string deneme;


using namespace std;

int main(int argc, char* argv[]) {
	readfile test1(argv[argc-2], argv[argc-1]);
	Notice_Stop_Observable* notice_=new Notice_Stop_Observable;
	engine testengine;
	
	while (1) {
		string a = test1.read_command();
		if (a[a.length() - 1] == ';');
		a.erase(a.length() - 1);
		string command;
		double value=-1;
		stringstream ss(a);
		int i=1;
		ss >> command >> value;
		if (command == "stop_simulation") {
			readfile::get_output_name() << "Simulation has stopped!!" << endl;
			notice_->addObserver(&testengine);
			notice_->notify_stop();
			for (int i = 0; i < testengine.tanks.size(); i++)
			{
				notice_->addObserver(&testengine.tanks.at(i));
				notice_->notify_stop();
			}

			break;
		}
					
		else if (command == "start_engine")
		{
			testengine.start_engine();
		}
		else if (command == "stop_engine")
		{
			testengine.stop_engine();
		}
		/*else if (command == "absorb_fuel")
			if (value == -1)
				readfile::get_output_name() << "Quantity not found at " << i << ". line"<<endl;
			else
				testengine.absorb_fuel(value);*/
		
		else if (command == "give_back_fuel")
			if (value == -1)
				readfile::get_output_name() << "Quantity not found at " << i << ". line" << endl;
			else
				testengine.give_back_fuel(value);
		
		else if (command == "add_fuel_tank")
			if (value == -1)
				readfile::get_output_name() << "Capacity not found at " << i << ". line" << endl;
			else
				testengine.add_fuel_tank(value);
		
		else if (command == "list_fuel_tanks")
			testengine.list_fuel_tank();
		
		else if (command == "remove_fuel_tank")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.remove_fuel_tank(value);
		
		else if (command == "connect_fuel_tank_to_engine")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.connect_fuel_tank_to_engine(value);
		
		else if (command == "disconnect_fuel_tank_from_engine")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.disconnect_fuel_tank_from_engine(value);
		
		else if (command == "open_valve")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.tanks.at(value).open_valve();
		
		else if (command == "close_valve")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.tanks.at(value).close_valve();
		
		else if (command == "break_fuel_tank")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.tanks.at(value).break_fuel_tank();
		
		else if (command == "repair_fuel_tank")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.tanks.at(value).repair_fuel_tank();

		else if (command == "print_fuel_tank_count")
			testengine.print_fuel_tank_count();

		else if (command == "list_connected_tanks")
			testengine.list_connected_tanks();

		else if (command == "print_total_fuel_quantity")
			testengine.print_total_fuel_quantity();

		else if (command == "print_total_consumed_fuel_quantity")
			testengine.print_total_consumed_fuel_quantity();

		else if (command == "print_tank_info")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
				testengine.print_tank_info(value);

		else if (command == "fill_tank")
			if (value == -1)
				readfile::get_output_name() << "Tank id not found at " << i << ". line" << endl;
			else
			{
				double value2;
				ss >> value2;
				testengine.fill_tank(value, value2);
			}

		else if (command == "wait")
			if (value == -1)
				readfile::get_output_name() << "There is not a time to wait " << i << ". line" << endl;
			else
				testengine.wait(value);
			
			
				
			


		i++;
	}
	
	return 0;
}