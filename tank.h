#pragma once
#include<stdio.h>
#include"Observer.h"
#include "Observable.h"
class tank:public Observer
{
private:
	Observable* observable;
	bool valve = 1; ///valve true is mean valve is open 
	double capacity;
	double fuel_quantity=0;
	bool broken=0; ///broken true is mean tank is broken
	bool tank_is_connected = 0;
	int id;

public:

	tank(double);
	bool is_valve_open();
	void open_valve();
	void close_valve();
	void break_fuel_tank();
	void repair_fuel_tank();
	void use_fuel();
	double get_fuel();
	void set_fuel(double);
	void print_info();
	void fill_fuel(double);
	void connect();
	void disconnect();
	bool is_connect();
	int get_id();
	
	~tank();
	void stop_info(string);
	void removeObserver(Observable* observable);
	void setObservable(Observable* observable);

};

