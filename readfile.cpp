#include "readfile.h"

readfile::readfile(string input, string output) {
	
	inputfile.open(input);
	if (!inputfile.is_open()) {
		cout << "File Cannot Open!!!";
		exit(1);
	}
	outputfile.open(output);
}
readfile::~readfile() {
	inputfile.close();
	outputfile.close();
}
string readfile::read_command() {
	string command;
	getline(inputfile, command);
	
	return command;
}

ofstream readfile::outputfile;


ofstream& readfile::get_output_name()
{
	return outputfile;
}

