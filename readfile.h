#pragma once
#include<iostream>
#include<string>
#include<fstream>
using namespace std;
class readfile
{
private:

	static ofstream outputfile;
	ifstream inputfile;
public:
	readfile(string,string);
	~readfile();
	string read_command();
	static ofstream& get_output_name();
	

};

